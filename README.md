# Hello World Example

Starts a FreeRTOS task to print "Hello World"

build with idf.py build
flash with idf.py flash
debug with idf.py monitor

exit monitor ctrl-t, crtl-]

Compiles with IDF version 3.3.2
git clone -b v3.3.2 --recursive https://github.com/espressif/esp-idf.git esp-idf-v3.3.2
cd esp-idf-v3.3.2/
Otherwise use the following to init the people tracking submodule
git submodule init
git submodule update