#include "Engine.h"

int Engine::getRpm() { return map(rpm, 0, 6450, 0x00, 0xFE); }

int Engine::getRpmSam() { return map(rpm, 0, 65535, 0x00, 65535); }

int Engine::getTemp() { return map(temp, -40, 215, 0, 0xFF); }

float Engine::map(float x, float in_min, float in_max, float out_min, float out_max) {
  if (x < in_min)
    return out_min;
  else if (x > in_max)
    return out_max;

  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

Engine::Engine(/* args */) {
  rpm = 0;
  batteryLevel = 50;
}

Engine::~Engine() {}
