#ifndef I2C_DAC_H
#define I2C_DAC_H

#include <stdint.h>
#include <stdio.h>

// #include "../Peripherals/mcp2515.h"
#include "io_base.h"
#include "mcp4725.h"

class MCP4728 : public IO_BASE {
 private:
  i2c_dev_t dev;
  const char* tag = "IO_dac";
#define NUMBER_OF_PINS 2

 public:
  MCP4728() : IO_BASE(NUMBER_OF_PINS) {}  // you can specify which base class constructor to call

  bool init(int firstPin, int address);
  void setGPIO(int pin, pinMode mode);
  void write(int pin, bool state);
  bool read(int pin);

  void writeADC(int pin, int value);
  int readADC(int pin) { return 0; };

  void addCallback(int pin, pinMode mode, void(*callback(void* arg))){};
};

#endif