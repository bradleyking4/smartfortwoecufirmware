#pragma once
#include <stdint.h>
#include <stdlib.h>
typedef enum pinMode {
  INPUT,
  INPUT_HIGH,
  INPUT_LOW,
  INPUT_ADC,
  OUTPUT,
  OUTPUT_HIGH,
  OUTPUT_LOW,
  INTERRUPT_RISING,
  INTERRUPT_FALLING,
  INTERRUPT_ANY,
} pinMode;

class IO_BASE {
 private:
 protected:
  pinMode* pins;

 public:
  uint8_t numberOfPins;
  uint8_t firstPin;

  virtual void setGPIO(int pin, pinMode mode);
  virtual void write(int pin, bool state);
  virtual bool read(int pin);

  virtual void writeADC(int pin, int value);
  virtual int readADC(int pin);

  virtual void addCallback(int pin, pinMode mode, void(*callback(void* arg)));

  IO_BASE();

  IO_BASE(int numberOfPins) {
    this->numberOfPins = numberOfPins;
    pins = (pinMode*)malloc(sizeof(pinMode) * numberOfPins);
  };
  ~IO_BASE() { free(pins); };
};
