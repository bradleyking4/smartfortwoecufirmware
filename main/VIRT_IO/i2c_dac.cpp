#include "i2c_dac.h"

#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <mcp4725.h>
#include <stdio.h>
#include <string.h>

#include "io_base.h"

#define LOG_LOCAL_LEVEL ESP_LOG_WARN
#include "../pinout.h"
#include "esp_log.h"
// A0, A1, A2 pins are grounded

#define SDA_GPIO SDA
#define SCL_GPIO SCL

static bool wait_for_eeprom(i2c_dev_t *dev)
{
  bool busy;
  while (true)
  {
    if ((mcp4725_eeprom_busy(dev, &busy) != ESP_OK))
    {
      return false;
    }
    if (!busy)
      return true;
    printf("...DAC is busy, waiting...\n");
    vTaskDelay(1);
  }
}

// MCP4728::MCP4728() { IO(NUMBER_OF_PINS); };

// we assumme that all virtual pins are continuous
bool MCP4728::init(int firstPin, int address)
{
  ESP_LOGI(tag, "initing dac at address %X, pins %d - %d", address, firstPin, firstPin + NUMBER_OF_PINS);

  this->firstPin = firstPin;

  memset(&dev, 0, sizeof(i2c_dev_t));
  if ((mcp4725_init_desc(&dev, 0, address, SDA_GPIO, SCL_GPIO)) != ESP_OK)
  {
    return false;
  }

  // mcp4725_power_mode_t pm;
  // ESP_ERROR_CHECK(mcp4725_get_power_mode(&dev, true, &pm));
  // if (pm != MCP4725_PM_NORMAL) {
  //   printf("DAC was sleeping... Wake up Neo!\n");
  //   ESP_ERROR_CHECK(mcp4725_set_power_mode(&dev, true, MCP4725_PM_NORMAL));
  //   wait_for_eeprom(&dev);
  // }

  printf("Set default DAC output value to 0...\n");

  if ((mcp4725_set_raw_output(&dev, 0, 0, true)))
  {
    return false;
  }

  return wait_for_eeprom(&dev);
}

void MCP4728::writeADC(int pin, int value)
{
  (mcp4725_set_raw_output(&dev, pin, value, false));
};

void MCP4728::write(int pin, bool state)
{
  ESP_LOGE(tag, "DAC Not Implemented");
}

bool MCP4728::read(int pin)
{
  ESP_LOGE(tag, "DAC Not Implemented");
  return 0;
}
void MCP4728::setGPIO(int pin, pinMode mode)
{
  ESP_LOGE(tag, "DAC Not Implemented");
};
