#include "i2c_expander.h"

#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "io_base.h"
#define CONFIG_MCP23X17_IFACE_I2C
#include <mcp23x17.h>
#include <stdio.h>
#include <string.h>

#define LOG_LOCAL_LEVEL ESP_LOG_WARN
#include "../pinout.h"
#include "esp_log.h"
// A0, A1, A2 pins are grounded

#define SDA_GPIO SDA
#define SCL_GPIO SCL

static void IRAM_ATTR intr_handler(void *arg) {
  char port = (long)arg;
  printf("int on port %c\n", port);
}

// MCP23x17::MCP23x17() { IO(NUMBER_OF_PINS); };

// we assumme that all virtual pins are continuous
bool MCP23x17::init(int firstPin, int address, gpio_num_t intA, gpio_num_t intB) {
  ESP_LOGI(tag, "initing expander at address %X, pins %d - %d", address, firstPin, firstPin + NUMBER_OF_PINS);

  this->firstPin = firstPin;

  memset(&dev, 0, sizeof(mcp23x17_t));
  ESP_ERROR_CHECK(mcp23x17_init_desc(&dev, 0, address, SDA_GPIO, SCL_GPIO));

  if (intA > 0) {
    gpio_install_isr_service(0);
    gpio_set_direction(intA, GPIO_MODE_INPUT);
    gpio_set_intr_type(intA, GPIO_INTR_POSEDGE);
    gpio_isr_handler_add(intA, intr_handler, (void *)'A');
  } else if (intB > 0) {
    gpio_install_isr_service(0);
    gpio_set_direction(intB, GPIO_MODE_INPUT);
    gpio_set_intr_type(intB, GPIO_INTR_POSEDGE);
    gpio_isr_handler_add(intB, intr_handler, (void *)'B');
  }
  return true;
}

void MCP23x17::write(int pin, bool state) {
  ESP_LOGI(tag, "setting %d to %d", pin, state);
  mcp23x17_set_level(&dev, pin, state);
}

bool MCP23x17::read(int pin) {
  uint32_t result;
  ESP_LOGI(tag, "read IO %d", pin);
  assert(pin >= 0 && pin <= NUMBER_OF_PINS);
  mcp23x17_get_level(&dev, pin, &result);
  return result;
}

void MCP23x17::setGPIO(int pin, pinMode mode) {
  ESP_LOGI(tag, "seting pin %d to %d", pin, mode);

  pins[pin] = mode;
  switch (mode) {
    case INPUT_LOW:
      // the mcp23017 does not have pull downs
    case INPUT:
      mcp23x17_set_mode(&dev, pin, MCP23X17_GPIO_INPUT);
      mcp23x17_set_pullup(&dev, pin, false);
      break;
    case INPUT_HIGH:
      mcp23x17_set_mode(&dev, pin, MCP23X17_GPIO_INPUT);
      mcp23x17_set_pullup(&dev, pin, true);
      break;
    case OUTPUT:
    case OUTPUT_LOW:
      mcp23x17_set_mode(&dev, pin, MCP23X17_GPIO_OUTPUT);
      mcp23x17_set_level(&dev, pin, false);
      break;
    case OUTPUT_HIGH:
      mcp23x17_set_mode(&dev, pin, MCP23X17_GPIO_OUTPUT);
      mcp23x17_set_level(&dev, pin, true);
      break;
    default:
      break;
  }
};

void MCP23x17::addCallback(int pin, pinMode mode, void(*callback(void *arg))) {
  switch (mode) {
    case INTERRUPT_ANY:
      mcp23x17_set_interrupt(&dev, pin, MCP23X17_INT_ANY_EDGE);
      break;

    case INTERRUPT_RISING:
      mcp23x17_set_interrupt(&dev, pin, MCP23X17_INT_HIGH_EDGE);
      break;

    case INTERRUPT_FALLING:
      mcp23x17_set_interrupt(&dev, pin, MCP23X17_INT_LOW_EDGE);
      break;

    default:
      mcp23x17_set_interrupt(&dev, pin, MCP23X17_INT_DISABLED);
      break;
  }
};
