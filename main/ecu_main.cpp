/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>

#include "Peripherals/can.h"
#include "Peripherals/canInternal.h"
#include "StateMachines/powerStateMachine.h"
#include "VIRT_IO/esp32_io.h"
#include "VIRT_IO/i2c_expander.h"
#include "VIRT_IO/virtual_IO.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "esp_spi_flash.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "pinout.h"

#define TIMER_WAKEUP_TIME_US (2 * 1000 * 1000)

static const char *TAG = "timer_wakeup";

MCP23x17 exp_gear, exp_kelly;
esp32_IO esp32_io;
extern "C"
{
  void app_main();
}

void init_pins()
{
  IO_setGPIO(BREAK_SW, OUTPUT_LOW);
  IO_setGPIO(ACCEL_SW, OUTPUT_LOW);
  IO_setGPIO(REV_SW, OUTPUT_LOW);
  IO_setGPIO(FWD_SW, OUTPUT_LOW);

  IO_setGPIO(CONTACTOR_MAIN, OUTPUT_LOW);
  IO_setGPIO(KELLY_POWER, OUTPUT_LOW);
  IO_setGPIO(CONTACTOR_MOTOR_DRIVE, OUTPUT_LOW);
  IO_setGPIO(CONTACTOR_CHARGE, OUTPUT_LOW);
  IO_setGPIO(CONTACTOR_DCDC, OUTPUT_LOW);

  IO_setGPIO(CONTACTOR_SPARE_1, OUTPUT_LOW);
  IO_setGPIO(CONTACTOR_SPARE_2, OUTPUT_LOW);

  IO_setGPIO(GEARBOX_MOTOR_DIR_A, OUTPUT_LOW);
  IO_setGPIO(GEARBOX_MOTOR_DIR_B, OUTPUT_LOW);

  IO_setGPIO(GEARSTICK_IOEXP_INT, INPUT);

  IO_setGPIO(ADC_GEAR_POS, INPUT_ADC);
  IO_setGPIO(ADC_GEAR_MOTOR_CURRENT, INPUT_ADC);
  IO_setGPIO(ADC_WATER_TEMPERATURE, INPUT_ADC);
  IO_setGPIO(ADC_OIL_TEMP, INPUT_ADC);
  IO_setGPIO(ADC_ACCEL_1, INPUT_ADC);
  IO_setGPIO(ADC_ACCEL_2, INPUT_ADC);

  IO_setGPIO(GEARSTICK_3, INPUT_HIGH);
  IO_setGPIO(GEARSTICK_4, INPUT_HIGH);
  IO_setGPIO(GEARSTICK_5, INPUT_HIGH);

  IO_setGPIO(GEARSTICK_8, INPUT_HIGH);
  IO_setGPIO(GEARSTICK_9, INPUT_HIGH);
  IO_setGPIO(GEARSTICK_10, INPUT_HIGH);

  IO_setGPIO(STOP_LIGHT, INPUT_HIGH);
  IO_setGPIO(STARTMOTOR, INPUT_HIGH);

  IO_setGPIO(LOCKOUT, INPUT);

  IO_setGPIO(IO_1, OUTPUT);
  IO_setGPIO(IO_2, INPUT);
  IO_setGPIO(IO_3, INPUT);
  IO_setGPIO(IO_4, INPUT);
}

esp_err_t example_register_timer_wakeup(void)
{
  esp_err_t ret = esp_sleep_enable_timer_wakeup(TIMER_WAKEUP_TIME_US);

  if (ret == ESP_OK)
  {
    ESP_LOGI(TAG, "timer wakeup source is ready");
  }
  else
  {
    ESP_LOGI(TAG, "Configure timer as wakeup source failed");
  }
  return ret;
}

inline void RelayOFF(int pin) { IO_write(pin, 0); }

inline void RelayON(int pin) { IO_write(pin, 1); }

void app_main()
{
  printf("Hello world!\n");

  /* Print chip information */
  esp_chip_info_t chip_info;
  esp_chip_info(&chip_info);
  printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ", chip_info.cores, (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
         (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

  printf("silicon revision %d, ", chip_info.revision);

  printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
         (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

  i2cdev_init();

  esp32_io.init();
  exp_gear.init(GPA0_1, 0x26, (gpio_num_t)0, (gpio_num_t)GEARSTICK_IOEXP_INT);
  exp_kelly.init(GPA0_2, 0x27, (gpio_num_t)0, (gpio_num_t)0);

  IO_addHandler(&esp32_io);
  IO_addHandler(&exp_gear);
  IO_addHandler(&exp_kelly);

  init_pins();
  TaskHandle_t canHandle = can_app_main();

  TaskHandle_t canInternalHandle = can_internal_app_main();
  car_init();
  bool init = false;

  example_register_timer_wakeup();
  uint8_t sleepCount = 0;
  while (1)
  {
    // IO_write(FWD_SW, 1);
    // IO_write(ACCEL_SW, 0);

    // vTaskDelay(10);
    // IO_write(ACCEL_SW, 1);
    // IO_write(FWD_SW, 0);

    vTaskDelay(pdMS_TO_TICKS(100));
    car_update();

    if (car_currentState() == CAR_OFF)
    {
      if (sleepCount < 3)
      {
        sleepCount++;
        printf("Attempting sleep %d\n", sleepCount);
      }
      else
      {
        vTaskSuspend(canHandle);
        vTaskSuspend(canInternalHandle);

        RelayOFF(CONTACTOR_SPARE_1);
        sleepCount = 0;
        printf("Sleeping\n");

        uart_wait_tx_idle_polling(CONFIG_ESP_CONSOLE_UART_NUM);

        esp_light_sleep_start();
        printf("Waking\n");

        smuStats.estopPressed = false;
        smuStats.smu_state = CHARGE_IDLE;

        vTaskDelay(pdMS_TO_TICKS(300));

        init = false;
      }
    }
    else
    {
      if (!init)
      {
        init = true;
      }

      vTaskResume(canHandle);
      vTaskResume(canInternalHandle);

      if (IO_read(STOP_LIGHT))
      {
        RelayON(CONTACTOR_SPARE_1);
      }
      else
      {
        RelayOFF(CONTACTOR_SPARE_1);
      }
    }
  }
}
