#include <stdint.h>
///
#pragma once
typedef enum SPEED_STATES
{
  SPEED__IDLE,
  SPEED_ACCEL,
  SPEED_REVERSE,
  SPEED_REGEN,
  SPEED_CHANGE,
  SPEED_NUM_STATES
} SPEED_STATES;

class STATE_MACHING_SPEED
{
private:
  SPEED_STATES currentState, newState;

  bool checkForTransitions(SPEED_STATES from);
  bool (*validTransition[SPEED_NUM_STATES][SPEED_NUM_STATES])();
  void createTransition(bool (*validTransition[SPEED_NUM_STATES][SPEED_NUM_STATES])(), SPEED_STATES from, SPEED_STATES to,
                        bool (*function)());

public:
  SPEED_STATES getState() { return currentState; };
  void speed_setupTransitions();
  void speed_init();
  void speed_update();
  void speed_paused(bool paused);

  uint8_t getAccelPos();
  uint16_t getKellyAccel();
};
