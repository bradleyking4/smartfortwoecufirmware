#include "speedStateMachine.h"

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "../VIRT_IO/virtual_IO.h"
#include "../gearbox.h"
#include "../pinout.h"
#include "esp_log.h"

#define ACCEL_PRESSED_THROTTLE 100 // mv
#define ACCEL_REGEN_POINT 100      // mv

inline void STATE_MACHING_SPEED::createTransition(bool (*validTransition[SPEED_NUM_STATES][SPEED_NUM_STATES])(), SPEED_STATES from,
                                                  SPEED_STATES to, bool (*function)())
{
  validTransition[from][to] = function;
}

const char *speedStrings[] = {"SPEED__IDLE", "SPEED_ACCEL", "SPEED_REVERSE", "SPEED_REGEN", "SPEED_CHANGE", "SPEED_NUM_STATES"};

int accelPedal;
int kellyAccel;

bool SpeedLocked = true;
/*Gear Change Completed*/
bool transistion_change_idle() { return true; }

/*accel pedal pressed*/
bool transistion_idle_accel() { return accelPedal > ACCEL_PRESSED_THROTTLE && gearbox.getPosition() > 1; }

/*brake down*/
bool transistion_idle_regen() { return IO_read(STOP_LIGHT); }

/*brake pedal up*/
bool transistion_regen_idle() { return !IO_read(STOP_LIGHT); }

/*more than 15% accel pedal pressed + break up*/
bool transistion_regen_accel() { return transistion_regen_idle() && accelPedal >= ACCEL_REGEN_POINT; }

/*less than 15% accel pedal pressed*/
bool transistion_accel_regen()
{
  if (IO_read(STOP_LIGHT))
  {
    return true;
  }
  return accelPedal < ACCEL_REGEN_POINT;
}

/*accel pedal up*/
bool transistion_accel_idle() { return accelPedal < ACCEL_PRESSED_THROTTLE; }

/*a gear stick change detected*/
bool transistion_gear_event() { return false; }

bool transistion_reverse_idle()
{
  return accelPedal < ACCEL_PRESSED_THROTTLE; // rpm = 0;
}

bool transistion_idle_reverse() { return accelPedal > ACCEL_PRESSED_THROTTLE && gearbox.getPosition() == 0; }

void STATE_MACHING_SPEED::speed_setupTransitions()
{
  memset(validTransition, 0, SPEED_NUM_STATES * SPEED_NUM_STATES);
  // from CAR_OFF to CAR_ACC
  createTransition(validTransition, SPEED_CHANGE, SPEED__IDLE, transistion_change_idle);

  createTransition(validTransition, SPEED__IDLE, SPEED_ACCEL, transistion_idle_accel);
  createTransition(validTransition, SPEED__IDLE, SPEED_REGEN, transistion_idle_regen);

  createTransition(validTransition, SPEED__IDLE, SPEED_REVERSE, transistion_idle_reverse);
  createTransition(validTransition, SPEED_REVERSE, SPEED__IDLE, transistion_reverse_idle);

  createTransition(validTransition, SPEED_REGEN, SPEED_ACCEL, transistion_regen_accel);
  createTransition(validTransition, SPEED_REGEN, SPEED__IDLE, transistion_regen_idle);

  createTransition(validTransition, SPEED_ACCEL, SPEED_REGEN, transistion_accel_regen);
  createTransition(validTransition, SPEED_ACCEL, SPEED__IDLE, transistion_accel_idle);

  createTransition(validTransition, SPEED__IDLE, SPEED_CHANGE, transistion_gear_event);
  createTransition(validTransition, SPEED_REGEN, SPEED_CHANGE, transistion_gear_event);
  createTransition(validTransition, SPEED_ACCEL, SPEED_CHANGE, transistion_gear_event);
}

bool STATE_MACHING_SPEED::checkForTransitions(SPEED_STATES from)
{
  if (SpeedLocked)
  {
    if (from != SPEED__IDLE)
    {
      ESP_LOGE("SPEED", "Forcing state %s->%s\n", speedStrings[newState], speedStrings[SPEED__IDLE]);
      newState = SPEED__IDLE;
      return true;
    }
    else
    {
      return false;
    }
  }
  for (int i = 0; i < SPEED_NUM_STATES; ++i)
  {
    if (validTransition[from][i] && (*validTransition[from][i])())
    {
      ESP_LOGE("SPEED", "Changing state %s->%s\n", speedStrings[newState], speedStrings[i]);
      newState = (SPEED_STATES)i;

      return true;
    }
  }
  return false;
}

void STATE_MACHING_SPEED::speed_init() { speed_setupTransitions(); }

void speed_changeStateFrom(SPEED_STATES from)
{
  switch (from)
  {
  case SPEED__IDLE:
    /* code */
    break;
  case SPEED_ACCEL:
    IO_writeADC(KELLY_DAC_THROTTLE, 0);
    IO_write(ACCEL_SW, 0);
    IO_write(FWD_SW, 0);

    break;
  case SPEED_REGEN:
    IO_writeADC(KELLY_DAC_BRAKE, 0);
    IO_write(BREAK_SW, 0);

    break;
  case SPEED_CHANGE:
    /* code */
    break;
  case SPEED_REVERSE:
    IO_write(ACCEL_SW, 0);
    IO_write(REV_SW, 0);
    break;
  default:
    break;
  }
}
void speed_changeStateTo(SPEED_STATES to)
{
  switch (to)
  {
  case SPEED__IDLE:
    /* code */
    break;
  case SPEED_ACCEL:
    IO_write(ACCEL_SW, 1);
    IO_write(FWD_SW, 1);

    break;
  case SPEED_REGEN:
    IO_write(BREAK_SW, 1);
    break;
  case SPEED_REVERSE:
    IO_write(ACCEL_SW, 1);
    IO_write(REV_SW, 1);
    break;
  case SPEED_CHANGE:
    /* code */
    break;
  default:
    break;
  }
}
void speed_handleState(SPEED_STATES state)
{
  switch (state)
  {
  case SPEED__IDLE:
    IO_write(ACCEL_SW, 0);
    IO_write(FWD_SW, 0);
    IO_write(REV_SW, 0);

    IO_writeADC(KELLY_DAC_THROTTLE, 0);

    break;
  case SPEED_REVERSE:
    printf("accel: %d\n", accelPedal);
    kellyAccel = (accelPedal * 4000 / 1500.0f) / 1.2;
    IO_writeADC(KELLY_DAC_THROTTLE, kellyAccel);
    break;
  case SPEED_ACCEL:
    printf("accel: %d\n", accelPedal);
    kellyAccel = accelPedal * 4000 / 1500.0f;
    IO_writeADC(KELLY_DAC_THROTTLE, kellyAccel);
    break;
  case SPEED_REGEN:
    IO_writeADC(KELLY_DAC_BRAKE, 1024);
    break;
  case SPEED_CHANGE:
    /* code */
    break;
  default:
    break;
  }
}

void STATE_MACHING_SPEED::speed_paused(bool paused)
{
  SpeedLocked = paused;
  speed_update();
}

void STATE_MACHING_SPEED::speed_update()
{
  int adc1 = IO_readADC(ADC_ACCEL_1);
  int adc2 = IO_readADC(ADC_ACCEL_2);

  // if (adc1 * 0.8 < adc2 && adc1 * 1.2 > adc2) {
  //   accelPedal = (adc1 + adc2) / 2;
  // } else {
  //   ESP_LOGE("SPEED", "Adc1 %d and Adc2 %d differ by 20", adc1, adc2);
  //   accelPedal = 0;
  // }

  accelPedal = (3100 - adc2) * 0.5 + accelPedal * 0.5;
  if (accelPedal < 0)
    accelPedal = 0;

  if (checkForTransitions(currentState))
  {
    speed_changeStateFrom(currentState);
    currentState = newState;
    speed_changeStateTo(currentState);
  }

  speed_handleState(currentState);
}

uint8_t STATE_MACHING_SPEED::getAccelPos() { return accelPedal >> 4; }

uint16_t STATE_MACHING_SPEED::getKellyAccel() { return kellyAccel; }
