#include "powerStateMachine.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#ifndef min
#define min(a, b) ((a) < (b) ? (a) : (b))
#endif

#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "../Engine.h"
#include "../Peripherals/can.h"
#include "../Peripherals/canInternal.h"
#include "../VIRT_IO/virtual_IO.h"
#include "../VIRT_IO/i2c_dac.h"

#include "../gearbox.h"
#include "../pinout.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "i2cdev.h"
#include "speedStateMachine.h"

CAR_STATES currentState, newState;
STATE_MACHING_SPEED speed;
Engine engine;
char lastGearstick = 'N';

CAR_STATES car_currentState() { return currentState; }

bool started = true;

bool kellyPower = false;
bool motorContactor = false;
bool enableMainContactor = false;
bool prechargeComplete = false;
bool smuPower = false;

MCP4728 dac;

bool (*validTransition[CAR_NUM_STATES][CAR_NUM_STATES])();

inline void createTransition(bool (*validTransition[CAR_NUM_STATES][CAR_NUM_STATES])(), CAR_STATES from, CAR_STATES to,
                             bool (*function)())
{
  validTransition[from][to] = function;
}

const char *stateStrings[] = {
    "CAR_OFF",
    "CAR_ACC",
    "CAR_START",
    "CAR_RUN",
    "CAR_STOP",
    "CAR_CHARGING",
    "CAR_NUM_STATES",
};
/*key in position 1*/
// not sure how to detect this.
bool transistion_off_acc()
{
  // return false;     //this stops the car from running
  return getCanStatus(); // || IO_read(STOP_LIGHT);
  // return IO_read(STOP_LIGHT);  // IO_read(GEARSTICK_9) ||
  // IO_read(GEARSTICK_4) || IO_read(GEARSTICK_5); return IO_read(STARTMOTOR);
  // // IO_read(GEARSTICK_9) || IO_read(GEARSTICK_4) || IO_read(GEARSTICK_5);
}

/*240v detected*/
bool transistion_off_charging() { return smuStats.smu_state == CHARGE_AC || smuStats.smu_state == CHARGE_J17772; }

/*240v no longer detected*/
bool transistion_charging_stop() { return smuStats.smu_state == CHARGE_IDLE; }

/*key in position 1*/
bool transistion_charging_acc() { return smuStats.smu_state == CHARGE_IDLE; }

/*240v detected*/
bool transistion_acc_charging() { return smuStats.smu_state == CHARGE_AC || smuStats.smu_state == CHARGE_J17772; }

/*key removed*/
bool transistion_acc_stop() { return !transistion_off_acc(); }

/*key in postion 2, and not charging*/
bool transistion_acc_start()
{
  // ESP_LOGW("PWR", "LastGearStick Test :%d ,%c", !(lastGearstick == 'N'),
  // lastGearstick);

  return kellyStats.Voltage > 90 && smuStats.smu_state == CHARGE_RUN;
  // return  // IO_read(STARTMOTOR);
  //     !(lastGearstick == 'N');
}

/*motor failed to start. Error state*/
bool transistion_start_acc() { return false; }

/*motor stated*/
bool transistion_start_run()
{
  if (smuStats.smu_state == CHARGE_ERROR)
  {
    return false;
  }
  bool initied = dac.init(KELLY_DAC_THROTTLE, 0x60);
  if (initied)
  {
    IO_writeADC(KELLY_DAC_THROTTLE, 0);
  }
  return initied;
}

/*rey removed and in n*/
bool transistion_run_stop() { return (smuStats.smu_state == CHARGE_ERROR) || ((lastGearstick == 'N') && !(getCanStatus())); }

/*is everything shutdown?*/
bool transistion_stop_off()
{
  vTaskDelay(pdMS_TO_TICKS(1000));

  return true; //! transistion_off_acc();
  // return !transistion_off_acc();
}

void car_setupTransitions()
{
  IO_addHandler(&dac);

  memset(validTransition, 0, CAR_NUM_STATES * CAR_NUM_STATES);
  // from CAR_OFF to CAR_ACC
  createTransition(validTransition, CAR_OFF, CAR_ACC, transistion_off_acc);
  createTransition(validTransition, CAR_OFF, CAR_CHARGING, transistion_off_charging);

  createTransition(validTransition, CAR_CHARGING, CAR_STOP, transistion_charging_stop);
  createTransition(validTransition, CAR_CHARGING, CAR_ACC, transistion_charging_acc);

  createTransition(validTransition, CAR_ACC, CAR_CHARGING, transistion_acc_charging);
  createTransition(validTransition, CAR_ACC, CAR_STOP, transistion_acc_stop);
  createTransition(validTransition, CAR_ACC, CAR_START, transistion_acc_start);

  createTransition(validTransition, CAR_START, CAR_ACC, transistion_start_acc);
  createTransition(validTransition, CAR_START, CAR_RUN, transistion_start_run);

  createTransition(validTransition, CAR_RUN, CAR_STOP, transistion_run_stop);
  createTransition(validTransition, CAR_STOP, CAR_OFF, transistion_stop_off);
}

void car_changeState(CAR_STATES _newState)
{
  ESP_LOGE("PWR", "Changing state %s->%s\n", stateStrings[newState], stateStrings[_newState]);
  newState = _newState;
}

bool checkForTransitions(CAR_STATES from)
{
  for (int i = 0; i < CAR_NUM_STATES; ++i)
  {
    if (validTransition[from][i] && (*validTransition[from][i])())
    {
      car_changeState((CAR_STATES)i);
      return true;
    }
  }
  return false;
}

inline void RelayOFF(int pin) { IO_write(pin, 0); }

inline void RelayON(int pin) { IO_write(pin, 1); }

void car_changeStateFrom(CAR_STATES from)
{
  switch (from)
  {
  case CAR_OFF:
    enableMainContactor = true;
    smuPower = true;
    RelayON(CONTACTOR_DCDC);
    RelayON(CONTACTOR_MAIN);

    break;
  case CAR_CHARGING:
    RelayOFF(CONTACTOR_CHARGE);
    prechargeComplete = false;

    break;
  case CAR_ACC:

    break;
  case CAR_START:

    break;
  case CAR_RUN:
    IO_write(KELLY_POWER, OFF);
    kellyPower = false;
    speed.speed_paused(true);
    break;
  case CAR_STOP:

    break;

  default:
    break;
  }
}

void car_handleState(CAR_STATES state)
{
  switch (state)
  {
  case CAR_OFF:

    break;
  case CAR_CHARGING:

    break;
  case CAR_ACC:
    // gearbox things

    // speed.speed_update();
    // engine.batteryLevel += 1;

    break;
  case CAR_START:
    vTaskDelay(1000 / portTICK_RATE_MS);
    // precharge and contactors
    // read kelly, if any faults set stating to false;

    break;
  case CAR_RUN:
    // engine.batteryLevel += 1;
    speed.speed_update();
    engine.rpm = kellyStats.rpm;

    // start sending RPM Stats.
    if (engine.rpm < 1000)
    {
      engine.rpm = 930; // (esp_random() * 20) / ((2 ^ 32) - 1);
    }
    engine.rpm = 900; // (esp_random() * 20) / ((2 ^ 32) - 1);

    break;
  case CAR_STOP:

    break;

  default:
    break;
  }
}

void car_changeStateTo(CAR_STATES to)
{
  switch (to)
  {

  case CAR_CHARGING:
    RelayON(CONTACTOR_MAIN);
    RelayON(CONTACTOR_CHARGE);
    prechargeComplete = true;
    break;
  case CAR_ACC:

    enableMainContactor = true;
    smuPower = true;
    RelayON(CONTACTOR_DCDC);
    RelayON(CONTACTOR_MAIN);

    IO_write(KELLY_POWER, ON);
    kellyPower = true;

    engine.batteryLevel = 0;
    engine.temp = 50;
    engine.rpm = 0;

    engine.oilLight = 1;
    engine.engineLight = 1;
    engine.batteryLight = 1;

    // full
    //  dashInfo.data[3] = 0b00000000;  // engine.batteryLevel;

    dashInfo.data[4] = 0b00000100; // engine.batteryLevel;//3 bulbs
                                   // dashInfo.data[4] = 0b00001000;  // engine.batteryLevel;//3 bulbs
                                   //  dashInfo.data[4] = 0b00010000;  // engine.batteryLevel;//3 bulbs
                                   //  dashInfo.data[4] = 0b00100000;  // engine.batteryLevel;//5 bulbs
    dashInfo.data[4] = 0b01000000; // engine.batteryLevel;//5 bulbs

    //  dashInfo.data[3] = 0b00000100;  // engine.batteryLevel; //1.5l
    //  dashInfo.data[3] = 0b00001000;  // engine.batteryLevel; //1l

    dashInfo.data[3] = 1 << 4; // engine.batteryLevel; //1l

    // 2
    //   dashInfo.data[3] = 0b00000010;  // engine.batteryLevel;
    //   dashInfo.data[3] = 0b01000000;  // engine.batteryLevel;

    // dashInfo.data[4] = 0b10000000;  // engine.batteryLevel;
    // engine.batteryLevel = dashInfo.data[3];
    break;
  case CAR_START:
    RelayON(CONTACTOR_MOTOR_DRIVE);
    motorContactor = true;

    // engine.batteryLevel = 0;
    engine.temp = 50;
    engine.oilLight = 0;
    engine.engineLight = 1;
    engine.batteryLight = 0;

    break;
  case CAR_RUN:

    // gearbox.changeTo(2);

    engine.oilLight = 0;
    engine.temp = 60;
    engine.engineLight = 0;

    speed.speed_paused(false);

    // dashInfo.data[3] = 0b00000001;  // engine.batteryLevel;
    // dashInfo.data[4] = 0b00010000;  // engine.batteryLevel;
    break;
  case CAR_STOP:
    memset(&kellyStats, 0, sizeof(kellyStats));
  case CAR_OFF:
    IO_write(KELLY_POWER, OFF);
    kellyPower = false;

    RelayOFF(CONTACTOR_CHARGE);
    prechargeComplete = false;

    speed.speed_paused(true);

    RelayOFF(CONTACTOR_SPARE_1);
    RelayOFF(CONTACTOR_SPARE_2);

    RelayOFF(CONTACTOR_MOTOR_DRIVE);
    motorContactor = false;

    smuPower = false;
    RelayOFF(CONTACTOR_DCDC);
    enableMainContactor = false;
    RelayOFF(CONTACTOR_MAIN);

    printf("PoweringOff\n");

    break;

  default:
    break;
  }
}

void car_init()
{
  currentState = CAR_OFF;
  newState = CAR_OFF;
  car_changeState(CAR_OFF);
  car_setupTransitions();
  car_changeStateTo(CAR_OFF);
  speed.speed_init();
}

long currentTime_ms;
long lastHeartbeat;
const long heartbeatInterval = 1000;

void car_update()
{
  if (checkForTransitions(currentState))
  {
    car_changeStateFrom(currentState);
    currentState = newState;
    car_changeStateTo(currentState);
  }

  char gearstick = 'N';
  if (IO_read(GEARSTICK_5))
  {
    if (IO_read(GEARSTICK_4))
    {
      gearstick = 'R';
    }
    else
    {
      gearstick = '+';
    }
  }
  else if (IO_read(GEARSTICK_4))
  {
    if (IO_read(GEARSTICK_3))
    {
      gearstick = '-';
    }
  }
  else if (IO_read(GEARSTICK_8))
  { // middle
    if (IO_read(GEARSTICK_3))
    {
      gearstick = 'D';
    }
    else
    {
      gearstick = 'N';
    }
  }
  else
  {
    gearstick = '?';
  }

  if (gearstick != lastGearstick)
  {
    switch (gearstick)
    {
    case '+':
      gearbox.changeUp();
      break;
    case 'D':
      /* code */
      break;
    case '-':
      gearbox.changeDown();
      break;
    case 'N':
      gearbox.changeTo(NETURAL);
      break;
    case 'R':
      gearbox.changeTo(REVERSE);
      break;
    case '?':
      /* code */
      break;
    default:
      break;
    }
    lastGearstick = gearstick;
  }
  // N Gearstick 3:0 4:1 5 lastGearstick =:0 8:1 9:1 10:0

  // up Gearstick = 5; 3:0 4:0 5:1 8:1 9:1 10:0
  // D Gearstick 3:1 4:0 5:0 8:1 9:1 10:0
  // down Gearstick  = 4; 3:1 4:1 5:0 8:0 9:1 10:0

  // rev Gearstick 3:1 4:1 5:0 8:0 9:1 10:0

  // rev?  Gearstick 3:0 4:1 5:1 8:0 9:1 10:0
  static uint8_t printLog = 0;
  printLog++;
  if (printLog == 20)
  {
    printf(
        "Gearstick 3:%d 4:%d 5:%d 8:%d 9:%d 10:%d   Gearstick:%c Gear:%d\n "
        "stop:%d starter:%d lockout:%d\n io3:%d io4:%d\n accel1:%d accel2:%d "
        "gearpos:%d gearcurrent:%d\nrpm :%d "
        "batteryLevel :%d kellyVoltage :%f\n",
        IO_read(GEARSTICK_3), IO_read(GEARSTICK_4), IO_read(GEARSTICK_5), IO_read(GEARSTICK_8), IO_read(GEARSTICK_9), IO_read(GEARSTICK_10),
        gearstick, gearbox.getPosition(), IO_read(STOP_LIGHT), IO_read(STARTMOTOR), IO_read(LOCKOUT), IO_read(IO_3), IO_read(IO_4),
        IO_readADC(ADC_ACCEL_1), IO_readADC(ADC_ACCEL_2), IO_readADC(ADC_GEAR_POS), IO_readADC(ADC_GEAR_MOTOR_CURRENT), engine.rpm,
        engine.batteryLevel, kellyStats.Voltage);
    printLog = 0;
  }

  engine.batteryLevel = engine.map(kellyStats.Voltage, engine.CELLS_LOW, engine.CELLS_MAX, 0, 5);

  dashInfo.data[0] = (gearbox.getPositionForDash() + 1) << 2;
  // dashInfo.data[0] |= 1 < 1;

  dashInfo.data[1] = engine.oilLight << 7;
  dashInfo.data[1] |= engine.engineLight << 6;
  dashInfo.data[1] |= engine.batteryLight << 1;

  // dashInfo.data[1] |= 1 << 5;  // no Clue

  // dashInfo.data[1] |= 1 << 4;  // no Clue
  // dashInfo.data[1] |= 1 << 3;  // no Clue

  // dashInfo.data[1] |= 1 << 2; //Radiatior Fan
  // dashInfo.data[1] |= engine.batteryLight << 0;  // alarm when car acc
  dashInfo.data[2] = engine.getRpm();

  // dashInfo.data[3] = engine.batteryLevel;  // engine.batteryLevel;
  dashInfo.data[3] = engine.batteryLevel << 4; // engine.batteryLevel;
  dashInfo.data[4] = 1 << 0;                   // engine.batteryLevel;

  // fuel 1
  // fuel 2 //
  // fuel 3 //190 [4]
  // fuel 4 //150 [4]
  // fuel 5
  // fuel 6
  // fuel 7
  // fuel 8

  engine.temp = kellyStats.cTemp > kellyStats.mTemp ? kellyStats.cTemp : kellyStats.mTemp;
  // engine temp -40 = 0, 215 = 2^8 -1
  dashInfo.data[5] = engine.getTemp();

  int rpmForSam = 900; // engine.getRpmSam();
  rpmForSam = engine.getRpmSam();

  // engineSpeed.data[0] = 0xFF;
  engineSpeed.data[1] = rpmForSam >> 8;
  engineSpeed.data[2] = rpmForSam & 255;

  engineSpeed.data[1] = 0;
  engineSpeed.data[2] = 0;

  engineSpeed.data[3] = gearbox.getNewPositionForSam() << 4 | gearbox.getNewPositionForSam();
  smuStatus.data[0] = enableMainContactor;

  currentTime_ms = esp_timer_get_time() / 1000;
  if (currentTime_ms > lastHeartbeat + heartbeatInterval)
  {
    lastHeartbeat = currentTime_ms;
    // heartbeat data
    hb.data[0] = currentState;
    hb.data[1] = speed.getState();

    hb.data[2] = (smuPower << 4) | (prechargeComplete << 3) | (enableMainContactor << 2) | (kellyPower << 1) | motorContactor;
    hb.data[3] = gearbox.getPosition();
    hb.data[4] = lastGearstick;

    hb.data[5] = min(speed.getAccelPos(), 100);
    hb.data[6] = min(speed.getKellyAccel() >> 8, 100);
    hb.data[7] = speed.getKellyAccel() & 0xff;

    can_transmit(&hb, 10);
  }

  car_handleState(currentState);
}