typedef enum CAR_STATES {
  CAR_OFF,
  CAR_ACC,
  CAR_START,
  CAR_RUN,
  CAR_STOP,
  CAR_CHARGING,
  CAR_NUM_STATES,
} CAR_STATES;

extern const char* stateStrings[];

void car_setupTransitions();

void car_init();
void car_update();

CAR_STATES car_currentState();
