
#include "../VIRT_IO/virtual_IO.h"
#include "../pinout.h"
#include "driver/ledc.h"
#include "stdint.h"

#define LEDC_TIMER LEDC_TIMER_0
#define LEDC_MODE LEDC_HIGH_SPEED_MODE
#define LEDC_OUTPUT_IO (16)  // Define the output GPIO
#define LEDC_CHANNEL LEDC_CHANNEL_0
#define LEDC_DUTY_RES LEDC_TIMER_12_BIT  // Set duty resolution to 12 bits
#define LEDC_FREQUENCY (15000)           // Frequency in Hertz. Set frequency at 15 kHz

// #define GEARBOX_MOTOR_DIR_A 2
// #define GEARBOX_MOTOR_DIR_B 2

static void example_ledc_init(void) {
  // Prepare and then apply the LEDC PWM timer configuration
  ledc_timer_config_t ledc_timer;
  ledc_timer.speed_mode = LEDC_MODE;
  ledc_timer.timer_num = LEDC_TIMER;
  ledc_timer.duty_resolution = LEDC_DUTY_RES;
  ledc_timer.freq_hz = LEDC_FREQUENCY;  // Set output frequency at 5 kHz
  ledc_timer.clk_cfg = LEDC_AUTO_CLK;
  ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

  // Prepare and then apply the LEDC PWM channel configuration
  ledc_channel_config_t ledc_channel;
  ledc_channel.speed_mode = LEDC_MODE;
  ledc_channel.channel = LEDC_CHANNEL;
  ledc_channel.timer_sel = LEDC_TIMER;
  ledc_channel.intr_type = LEDC_INTR_DISABLE;
  ledc_channel.gpio_num = LEDC_OUTPUT_IO;
  ledc_channel.duty = 0;  // Set duty to 0%
  ledc_channel.hpoint = 0;
  ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));
}

void Motor_SetDirection(int8_t dir) {
  if (dir == 1) {
    IO_setGPIO(GEARBOX_MOTOR_DIR_A, OUTPUT_HIGH);
    IO_setGPIO(GEARBOX_MOTOR_DIR_B, OUTPUT_LOW);

  } else if (dir == -1) {
    IO_setGPIO(GEARBOX_MOTOR_DIR_B, OUTPUT_HIGH);
    IO_setGPIO(GEARBOX_MOTOR_DIR_A, OUTPUT_LOW);

  } else {
    IO_setGPIO(GEARBOX_MOTOR_DIR_A, OUTPUT_LOW);
    IO_setGPIO(GEARBOX_MOTOR_DIR_B, OUTPUT_LOW);
    ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, 0);
    ledc_update_duty(LEDC_MODE, LEDC_CHANNEL);
  }
}

//set the duty cycle of the motor.
void Motor_setDuty(uint8_t duty) {
  ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, duty);
  ledc_update_duty(LEDC_MODE, LEDC_CHANNEL);
}