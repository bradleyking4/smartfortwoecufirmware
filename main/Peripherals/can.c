/* CAN Network Slave Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*
 * The following example demonstrates a slave node in a CAN network. The slave
 * node is responsible for sending data messages to the master. The example will
 * execute multiple iterations, with each iteration the slave node will do the
 * following:
 * 1) Start the CAN driver
 * 2) Listen for ping messages from master, and send ping response
 * 3) Listen for start command from master
 * 4) Send data messages to master and listen for stop command
 * 5) Send stop response to master
 * 6) Stop the CAN driver
 */

#include "driver/can.h"

#include <stdio.h>
#include <stdlib.h>

#include "can.h"
#include "esp_err.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

/* --------------------- Definitions and static variables ------------------ */
// Example Configuration
#define DATA_PERIOD_MS 50
#define NO_OF_ITERS 3
#define ITER_DELAY_MS 1000
#define RX_TASK_PRIO 8    // Receiving task priority
#define TX_TASK_PRIO 9    // Sending task priority
#define CTRL_TSK_PRIO 10  // Control task priority
#define TX_GPIO_NUM 23
#define RX_GPIO_NUM 22
#define EXAMPLE_TAG "CAN Slave"

#define ID_MASTER_STOP_CMD 0x0A0
#define ID_MASTER_START_CMD 0x0A1
#define ID_MASTER_PING 0x0A2
#define ID_SLAVE_STOP_RESP 0x0B0
#define ID_SLAVE_DATA 0x0B1
#define ID_SLAVE_PING_RESP 0x0B2

ESPSTATUS espStatus;

void processCanMessage(can_message_t message);

static const can_general_config_t g_config = CAN_GENERAL_CONFIG_DEFAULT(TX_GPIO_NUM, RX_GPIO_NUM, CAN_MODE_NORMAL);
static const can_timing_config_t t_config = CAN_TIMING_CONFIG_500KBITS();
static const can_filter_config_t f_config = CAN_FILTER_CONFIG_ACCEPT_ALL();

// can_message_t hb = {.identifier = 0x440, .data_length_code = 1, .data = {0,
// 0, 0, 0, 0, 0, 0, 0}}; can_message_t smuStatus = {.identifier = 0x441,
// .data_length_code = 1, .data = {0, 0, 0, 0, 0, 0, 0, 0}};

// smart car payloads
can_message_t status = {.identifier = 0x510, .data_length_code = 1, .data = {~((uint8_t)1 << 7), 0, 0, 0, 0, 0, 0, ~((uint8_t)1 << 7)}};
// Data bytes of data message will be initialized in the transmit task
// can_message_t data_message = {.identifier = ID_SLAVE_DATA, .data_length_code
// = 4, .data = {0, 0, 0, 0, 0, 0, 0, 0}};

uint8_t vin1[8] = {1, '4', '5', '0', '3', '3', '2', '2'};
uint8_t vin2[8] = {2, 'J', '0', '2', '3', '4', '5', '4'};
uint8_t vin3[8] = {3, 0, 0, 0, 4, 169, 128, 26};
can_message_t vin = {.identifier = 0x560, .data_length_code = 8};

QueueHandle_t can_car_send_queue;
// gear indicator 1 << 3 = N

// Dash Display
//   0 = P
//  1 = R     1st
//  2 = N
//  3 = D
//  4 = 1st
//  5 = 2nd
//  6 = 3
//  7 = 4
//  8 = 5
//  9 = 6
//  10 = W
//  11 = S
//  12 = E
//  13 = ""
//  14 = -
//  15 = =

can_message_t dashInfo = {.identifier = 0x190, .data_length_code = 6, .data = {2 << 2, 0, 0, 0, 0, 0, 0, 0}};

can_message_t speedo = {.identifier = 0x080,
                        .data_length_code = 3,
                        // .data = {47856 >> 8, 47856 & 0xFF, 1 << 7, 0, 0, 0, 0, 0}};
                        .data = {15, 5, 1, 0, 0, 0, 0, 0}};

can_message_t engineSpeed = {.identifier = 0x300, .data_length_code = 4, .data = {0, 0, 0, 7 << 4 | 7, 0, 0, 0, 0}};

/* --------------------------- Tasks and Functions -------------------------- */

volatile int canAlive = false;

bool getCanStatus() { return canAlive; }

static void can_send_status_updates(void *arg) {
  while (1) {
    if (canAlive) {
      // printf("sending status");
      can_message_t *cmPtr = &dashInfo;
      xQueueSend(can_car_send_queue, &cmPtr, portMAX_DELAY);
      vTaskDelay(1);

      cmPtr = &status;
      xQueueSend(can_car_send_queue, &cmPtr, portMAX_DELAY);
      vTaskDelay(1);

      // sent from abs controller???
      //  cmPtr = &speedo;
      //  xQueueSend(can_car_send_queue, &cmPtr, portMAX_DELAY);
      //  vTaskDelay(5);

      cmPtr = &engineSpeed;
      xQueueSend(can_car_send_queue, &cmPtr, portMAX_DELAY);
      vTaskDelay(1);

      for (int i = 0; i < 8; i++) {
        vin.data[i] = vin1[i];
      }
      cmPtr = &vin;
      xQueueSend(can_car_send_queue, &cmPtr, portMAX_DELAY);
      vTaskDelay(pdMS_TO_TICKS(1));

      for (int i = 0; i < 8; i++) {
        vin.data[i] = vin2[i];
      }
      cmPtr = &vin;
      xQueueSend(can_car_send_queue, &cmPtr, portMAX_DELAY);
      vTaskDelay(pdMS_TO_TICKS(1));
      for (int i = 0; i < 8; i++) {
        vin.data[i] = vin3[i];
      }
      cmPtr = &vin;
      xQueueSend(can_car_send_queue, &cmPtr, portMAX_DELAY);
    }
    vTaskDelay(pdMS_TO_TICKS(60));
    /* code */
  }
}

static void can_transmit_task(void *arg) {
  can_message_t message, *sendMessage;
  int canFailedCount = 0;
  while (1) {
    while (!(can_receive(&message, pdMS_TO_TICKS(100)) == ESP_OK))
      ;
    canAlive = true;
    canFailedCount = 0;
    for (int i = 0; i < 8; i++) {
      vin.data[i] = vin1[i];
    }
    can_transmit(&vin, 100);
    vTaskDelay(pdMS_TO_TICKS(5));

    for (int i = 0; i < 8; i++) {
      vin.data[i] = vin2[i];
    }
    can_transmit(&vin, 100);
    vTaskDelay(pdMS_TO_TICKS(5));
    for (int i = 0; i < 8; i++) {
      vin.data[i] = vin3[i];
    }
    can_transmit(&vin, 100);

    while (canAlive) {
      if (xQueueReceive(can_car_send_queue, &sendMessage, 0) == pdTRUE) {
        can_transmit(sendMessage, 10);
      }

      if (can_receive(&message, pdMS_TO_TICKS(100)) == ESP_OK) {
        processCanMessage(message);
        canFailedCount = 0;
        // printf("Message received\n");

      } else {
        canFailedCount++;
        if (canFailedCount > 10) {
          canAlive = false;
          printf("Failed to receive message\n");
        }

        continue;
      }

      // Process received message
      if (message.flags & CAN_MSG_FLAG_EXTD) {
        printf("Message is in Extended Format\n");
      } else {
        // printf("Message is in Standard Format\n");
      }
    }
  }
  vTaskDelete(NULL);
}

// 0, ID
// 1, count received
// 2, size,
// 3-10, data
int canIds[20][11];
int canIDCount = 0;

void processCanMessage(can_message_t message) {
  switch (message.identifier) {
    case 0x090:
      espStatus.espActive = (message.data[0] >> 4) & 1;
      espStatus.espDisabled = (message.data[0] >> 2) & 1;
      espStatus.ABSon = (message.data[0] >> 0) & 1;

      espStatus.brakeSwitch = (message.data[1] >> 2) & 1;
      espStatus.ABSon2 = (message.data[1] >> 0) & 1;
      break;
    default:
      break;
  }

  for (int i = 0; i < canIDCount; i++) {
    if (canIds[i][0] == message.identifier) {
      canIds[i][1]++;

      if (message.data_length_code < 9) {
        canIds[i][2] = message.data_length_code;

        if (!(message.flags & CAN_MSG_FLAG_RTR)) {
          for (int j = 0; j < message.data_length_code; j++) {
            canIds[i][3 + j] = message.data[j];
          }
        }
      }

      return;
    }
  }

  // didn't find the message
  canIds[canIDCount][0] = message.identifier;
  canIds[canIDCount][1] = 0;
  canIds[canIDCount][2] = 0;

  for (int j = 0; j < 8; j++) {
    canIds[canIDCount][3 + j] = 0;
  }

  if (canIDCount < 20) canIDCount++;

  // printf("ID is %d\n", message.identifier);
  // if (!(message.flags & CAN_MSG_FLAG_RTR)) {
  //   for (int i = 0; i < message.data_length_code; i++) {
  //     printf("Data byte %d = %d\n", i, message.data[i]);
  //   }
  // }
}

static void can_print_can_messages(void *arg) {
  while (1) {
    printf("number of can Id's %d \n", canIDCount);
    for (int i = 0; i < canIDCount; i++) {
      printf("%#06X %d\t%d", canIds[i][0], canIds[i][1], canIds[i][2]);
      for (int j = 0; j < canIds[i][2]; j++) {
        printf("\t%d", canIds[i][3 + j]);
      }
      printf("\n");
    }

    printf((espStatus.espActive ? "espActive " : ""));
    printf((espStatus.espDisabled ? "espDisabled " : "espEnabled "));
    printf(espStatus.brakeSwitch ? "brakeSwitch " : "");
    printf(((espStatus.ABSon) ? "ABSon " : ""));
    printf(((espStatus.ABSon2) ? "ABSon2" : ""));
    printf("\n");
    vTaskDelay(pdMS_TO_TICKS(10000));
  }
}

TaskHandle_t can_app_main(void) {
  // Add short delay to allow master it to initialize first

  // Install CAN driver, trigger tasks to start
  // Install CAN driver

  if (can_driver_install(&g_config, &t_config, &f_config) == ESP_OK) {
    printf("Driver installed\n");
  } else {
    printf("Failed to install driver\n");
    return;
  }

  if (can_start() == ESP_OK) {
    printf("Driver started\n");
  } else {
    printf("Failed to start driver\n");
    return;
  }
  can_message_t message;
  can_car_send_queue = xQueueCreate(10, sizeof(&message));

  TaskHandle_t canHandle, canStatusHandle;
  xTaskCreatePinnedToCore(can_transmit_task, "CAN_tx", 4096, NULL, CTRL_TSK_PRIO, &canHandle, 1);
  xTaskCreatePinnedToCore(can_send_status_updates, "CAN_status", 2048, NULL, TX_TASK_PRIO, &canStatusHandle, 1);

  xTaskCreatePinnedToCore(can_print_can_messages, "CAN_print", 2048, NULL, TX_TASK_PRIO - 1, NULL, 1);

  // // Uninstall CAN driver
  // ESP_ERROR_CHECK(can_driver_uninstall());
  // ESP_LOGI(EXAMPLE_TAG, "Driver uninstalled");

  // // Cleanup
  // vSemaphoreDelete(ctrl_task_sem);
  // vSemaphoreDelete(stop_data_sem);
  // vSemaphoreDelete(done_sem);
  // vQueueDelete(tx_task_queue);
  // vQueueDelete(rx_task_queue);
  return canStatusHandle;
}
