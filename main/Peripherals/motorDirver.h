#pragma once

#include "driver/can.h"

#ifdef __cplusplus
extern "C" {
#endif

void Motor_SetDirection(int8_t dir);
void Motor_setDuty(uint8_t duty);

#ifdef __cplusplus
}
#endif