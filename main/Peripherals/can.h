#pragma once

#include "driver/can.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#ifdef __cplusplus
extern "C" {
#endif

TaskHandle_t can_app_main();

extern can_message_t dashInfo;
extern can_message_t speedo;
extern can_message_t engineSpeed;

extern can_message_t sam;

extern can_message_t smuStatus;
extern can_message_t hb;

typedef struct ESPSTATUS {
  bool espActive;
  bool espDisabled;
  bool brakeSwitch;
  bool ABSon;
  bool ABSon2;

} ESPSTATUS;

bool getCanStatus();

#ifdef __cplusplus
}
#endif