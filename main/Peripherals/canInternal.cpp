/* CAN Network Slave Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*
 * The following example demonstrates a slave node in a CAN network. The slave
 * node is responsible for sending data messages to the master. The example will
 * execute multiple iterations, with each iteration the slave node will do the
 * following:
 * 1) Start the CAN driver
 * 2) Listen for ping messages from master, and send ping response
 * 3) Listen for start command from master
 * 4) Send data messages to master and listen for stop command
 * 5) Send stop response to master
 * 6) Stop the CAN driver
 */

#include "canInternal.h"

#include <stdio.h>
#include <stdlib.h>

#include "driver/can.h"
#include "driver/spi_common.h"
#include "esp32-mcp2515/mcp2515.h"
#include "esp_err.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

/* --------------------- Definitions and static variables ------------------ */
// Example Configuration
#define DATA_PERIOD_MS 50
#define NO_OF_ITERS 3
#define ITER_DELAY_MS 1000
#define RX_TASK_PRIO 8   // Receiving task priority
#define TX_TASK_PRIO 9   // Sending task priority
#define CTRL_TSK_PRIO 10 // Control task priority

#define EXAMPLE_TAG "CAN Slave"

#define ID_MASTER_STOP_CMD 0x0A0
#define ID_MASTER_START_CMD 0x0A1
#define ID_MASTER_PING 0x0A2
#define ID_SLAVE_STOP_RESP 0x0B0
#define ID_SLAVE_DATA 0x0B1
#define ID_SLAVE_PING_RESP 0x0B2

void processCanMessage(can_message_t message);

#define PIN_NUM_MISO 12
#define PIN_NUM_MOSI 13
#define PIN_NUM_CLK 14
#define PIN_NUM_CS 17

can_message_t hb = {.identifier = 0x440, .data_length_code = 8, .data = {0, 0, 0, 0, 0, 0, 0, 0}};
can_message_t smuStatus = {.identifier = 0x441, .data_length_code = 8, .data = {0, 0, 0, 0, 0, 0, 0, 0}};

// smart car payloads
// Data bytes of data message will be initialized in the transmit task
// can_message_t data_message = {.identifier = ID_SLAVE_DATA, .data_length_code
// = 4, .data = {0, 0, 0, 0, 0, 0, 0, 0}};

QueueHandle_t can_kelly_send_queue;

KellyStats kellyStats;
SMUstatus smuStats;
/* --------------------------- Tasks and Functions -------------------------- */

volatile int internalCanAlive = true;

bool getCanInternalStatus() { return internalCanAlive; }

static void can_send_status_updates(void *arg)
{
  int counter = 0;
  while (1)
  {
    vTaskDelay(pdMS_TO_TICKS(100));
    if (counter > 10)
    {
      printf("rpm = %d, voltage: %f, current: %f  cTemp: %d mTemp: %d \n", kellyStats.rpm, kellyStats.Voltage, kellyStats.Current,
             kellyStats.cTemp, kellyStats.mTemp);
      counter = 0;
    }
    counter++;

    if (internalCanAlive)
    {
      // printf("sending status");
      can_message_t *cmPtr = &smuStatus;
      xQueueSend(can_kelly_send_queue, &cmPtr, portMAX_DELAY);
      vTaskDelay(5);

      cmPtr = &hb;
      xQueueSend(can_kelly_send_queue, &cmPtr, portMAX_DELAY);
      vTaskDelay(5);
    }
    /* code */
  }
}

static void can_transmit_task(void *arg)
{
  esp_err_t ret;

  spi_bus_config_t buscfg = {
      .mosi_io_num = PIN_NUM_MOSI,
      .miso_io_num = PIN_NUM_MISO,
      .sclk_io_num = PIN_NUM_CLK,
      .quadwp_io_num = -1,
      .quadhd_io_num = -1,
      .max_transfer_sz = 32,
  };

  spi_device_interface_config_t devcfg = {
      .mode = 0,                         // SPI mode 0
      .clock_speed_hz = 1 * 1000 * 1000, // Clock out at 10 MHz
      .spics_io_num = PIN_NUM_CS,        // CS pin
      .queue_size = 1,                   // We want to be able to queue 7 transactions at a time
  };
  spi_device_handle_t spi;

  ret = spi_bus_initialize(VSPI_HOST, &buscfg, 1);
  ESP_ERROR_CHECK(ret);

  ret = spi_bus_add_device(VSPI_HOST, &devcfg, &spi);
  ESP_ERROR_CHECK(ret);

  MCP2515 mcp2515(&spi);
  mcp2515.reset();
  mcp2515.setBitrate(CAN_250KBPS, MCP_8MHZ);
  mcp2515.setNormalMode();

  can_message_t message, *sendMessage;
  int canFailedCount = 0;
  while (1)
  {
    // while (!(mcp2515.readMessage(&message) == MCP2515::ERROR_OK))
    //   ;
    internalCanAlive = true;

    // for (int i = 0; i < 8; i++) {
    //   vin.data[i] = vin1[i];
    // }
    // can_transmit(&vin, 100);
    // vTaskDelay(pdMS_TO_TICKS(5));

    // for (int i = 0; i < 8; i++) {
    //   vin.data[i] = vin2[i];
    // }
    // can_transmit(&vin, 100);
    // vTaskDelay(pdMS_TO_TICKS(5));
    // for (int i = 0; i < 8; i++) {
    //   vin.data[i] = vin3[i];
    // }
    // can_transmit(&vin, 100);

    while (internalCanAlive)
    {
      if (xQueueReceive(can_kelly_send_queue, &sendMessage, 0) == pdTRUE)
      {
        MCP2515::ERROR ret = mcp2515.sendMessage(sendMessage);
        if (ret != MCP2515::ERROR_OK)
        {
          printf("internal sendMessage Failed %d\n", (int)ret);
        }
      }

      if (mcp2515.checkReceive())
      {
        if (mcp2515.readMessage(&message) == MCP2515::ERROR_OK)
        {
          // processCanMessage(message);
          canFailedCount = 0;
          // printf("Message received\n");

          switch (message.identifier)
          {
          case 0x420:

            smuStats.smu_state = (SMU_STATES)message.data[0];
            smuStats.estopPressed = message.data[1];

            break;
          case 0x423:
            break;

          case 0x430:
            break;

          case 217128453UL:
            kellyStats.rpm = message.data[1] * 256 + message.data[0];
            kellyStats.Current = (message.data[3] * 256 + message.data[2]) / 10.0;
            kellyStats.Voltage = (message.data[5] * 256 + message.data[4]) / 10.0;
            // printf("Got a thing 217128453UL\n");
            break;

          case 217128709UL:
            kellyStats.cTemp = message.data[1] - 40;
            kellyStats.mTemp = message.data[2] - 30;
            kellyStats.throttle = message.data[0] / 256.0;
            // printf("Got a thing 217128709UL\n");
            // 217128709UL : {  // ID（0x0CF11F05）
            //   Serial.print(" Throttle: ");
            //   float throttle = frame.data[0] / 256.0;
            //   Serial.print(throttle);
            //   Serial.print(" Controller Temp: ");
            //   uint8_t cTemp = frame.data[1] - 40;
            //   Serial.print(cTemp);
            //   Serial.print(" Motor Temp: ");
            //   uint8_t mTemp = frame.data[2] - 30;
            //   Serial.println(mTemp);

            //   // 4 is reserved
            //   uint8_t cStatus = frame.data[5];
            //   uint8_t switchStatus = frame.data[6];
            //   // bit0 - hallA
            //   // bit1 - hallB
            //   // bit2 - hallC
            //   // bit3 - 12v break
            //   // bit4 - reverse Switch
            //   // bit5 - forwards
            //   // bit6 - foot switch
            //   // bit7 - boost
            // }
            break;

          default:
            printf("ID is %X", message.identifier);
            if (message.flags & CAN_MSG_FLAG_EXTD)
            {
              printf(" - Extended Format\n");
            }
            else
            {
              printf("\n");
            }

            break;
          }

          // 217128453UL : {  // 0CF11E05
          //   Serial.print(" RPM: ");
          //   int speed = frame.data[1] * 256 + frame.data[0];
          //   Serial.print(speed);
          //   Serial.print(" Current: ");
          //   float current = (frame.data[3] * 256 + frame.data[2]) /
          //   10; Serial.print(current); Serial.print(" Voltage: ");
          //   float voltage = (frame.data[5] * 256 + frame.data[4]) /
          //   10; Serial.print(voltage); Serial.print(" Error: ");
          //   int error = frame.data[7] * 256 + frame.data[6];
        }
        else
        {
          canFailedCount++;
          // if (canFailedCount > 10) {
          //   internalCanAlive = false;
          //   printf("Can Internal Failed to receive message\n");
          // }

          continue;
        }
      }
      else
      {
        vTaskDelay(10);
      }

      // // Process received message
      // if (message.flags & CAN_MSG_FLAG_EXTD) {
      //   printf("Message is in Extended Format\n");
      // } else {
      //   // printf("Message is in Standard Format\n");
      // }
    }
  }
  vTaskDelete(NULL);
}

// 0, ID
// 1, count received
// 2, size,
// 3-10, data
// int canIds[20][11];
// int canIDCount = 0;

// void processCanMessage(can_message_t message) {
//   for (int i = 0; i < canIDCount; i++) {
//     if (canIds[i][0] == message.identifier) {
//       canIds[i][1]++;

//       if (message.data_length_code < 9) {
//         canIds[i][2] = message.data_length_code;

//         if (!(message.flags & CAN_MSG_FLAG_RTR)) {
//           for (int j = 0; j < message.data_length_code; j++) {
//             canIds[i][3 + j] = message.data[j];
//           }
//         }
//       }

//       return;
//     }
//   }

//   // didn't find the message
//   canIds[canIDCount][0] = message.identifier;
//   canIds[canIDCount][1] = 0;
//   canIds[canIDCount][2] = 0;

//   if (canIDCount < 20) canIDCount++;

//   // printf("ID is %d\n", message.identifier);
//   // if (!(message.flags & CAN_MSG_FLAG_RTR)) {
//   //   for (int i = 0; i < message.data_length_code; i++) {
//   //     printf("Data byte %d = %d\n", i, message.data[i]);
//   //   }
//   // }

//   switch (message.identifier) {
//     default:
//       break;
//   }
// }

// static void can_print_can_messages(void *arg) {
//   while (1) {
//     printf("number of can Id's %d \n", canIDCount);
//     for (int i = 0; i < canIDCount; i++) {
//       printf("%#06X %d", canIds[i][0], canIds[i][1]);
//       for (int j = 0; j < canIds[i][2]; j++) {
//         printf("\t%d", canIds[i][3 + j]);
//       }
//       printf("\n");
//     }

//     vTaskDelay(pdMS_TO_TICKS(10000));
//   }
// }

TaskHandle_t can_internal_app_main(void)
{
  // Add short delay to allow master it to initialize first

  // Install CAN driver, trigger tasks to start
  // Install CAN driver

  //   if (can_driver_install(&g_config, &t_config, &f_config) == ESP_OK) {
  //   printf("Driver installed\n");
  // } else {
  //   printf("Failed to install driver\n");
  //   return;
  // }

  // if (can_start() == ESP_OK) {
  //   printf("Driver started\n");
  // } else {
  //   printf("Failed to start driver\n");
  //   return;
  // }

  can_message_t message;

  can_kelly_send_queue = xQueueCreate(10, sizeof(&message));

  TaskHandle_t canHandle, canStatusHandle;
  xTaskCreatePinnedToCore(can_transmit_task, "CAN_Internal_tx", 4096, NULL, CTRL_TSK_PRIO, &canHandle, 1);
  xTaskCreatePinnedToCore(can_send_status_updates, "CAN_Internal_status", 2048, NULL, TX_TASK_PRIO, &canStatusHandle, 1);

  // xTaskCreatePinnedToCore(can_print_can_messages, "CAN_print", 2048, NULL,
  // TX_TASK_PRIO - 1, NULL, 1);

  // // Uninstall CAN driver
  // ESP_ERROR_CHECK(can_driver_uninstall());
  // ESP_LOGI(EXAMPLE_TAG, "Driver uninstalled");

  // // Cleanup
  // vSemaphoreDelete(ctrl_task_sem);
  // vSemaphoreDelete(stop_data_sem);
  // vSemaphoreDelete(done_sem);
  // vQueueDelete(tx_task_queue);
  // vQueueDelete(rx_task_queue);
  return canHandle;
}
