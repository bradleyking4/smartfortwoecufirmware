#pragma once

#include "driver/can.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#ifdef __cplusplus
extern "C" {
#endif

TaskHandle_t can_internal_app_main();

extern can_message_t dashInfo;
extern can_message_t speedo;
extern can_message_t engineSpeed;

extern can_message_t sam;

extern can_message_t smuStatus;
extern can_message_t hb;

typedef struct KellyStats {
  float Current;
  float Voltage;
  uint16_t rpm;
  uint16_t error;
  float throttle;
  uint8_t cTemp;
  uint8_t mTemp;
} KellyStats;

extern KellyStats kellyStats;

typedef enum SMU_STATES {
  CHARGE_IDLE,
  CHARGE_J17772,
  CHARGE_AC,
  CHARGE_PRE,
  CHARGE_RUN,
  CHARGE_ERROR,
  CHARGE_NUM_STATES,
} SMU_STATES;

typedef struct SMUstatus {
  bool estopPressed = 0;
  SMU_STATES smu_state = CHARGE_IDLE;
  /* data */
} SMUstatus;

extern SMUstatus smuStats;

bool getCanInternalStatus();

#ifdef __cplusplus
}
#endif