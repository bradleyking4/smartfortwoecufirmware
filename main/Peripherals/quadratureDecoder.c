#include <esp_log.h>
#include <stdio.h>

#include "../pinout.h"
#include "../taskDefs.h"
#include "driver/gpio.h"
#include "driver/pcnt.h"
#include "driver/periph_ctrl.h"
#include "esp_types.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"

#define PCNT_PULSE_GPIO GEARBOX_ENCODER_A  // gpio for PCNT
#define PCNT_CONTROL_GPIO GEARBOX_ENCODER_B

#define PCNT_H_LIM_VAL 1000
#define PCNT_L_LIM_VAL -1000

// typedef enum { QUAD_ENC_MODE_1 = 1, QUAD_ENC_MODE_2 = 2, QUAD_ENC_MODE_4 = 4 } quad_encoder_mode;

// xQueueHandle pcnt_evt_queue;  // A queue to handle pulse counter events

// /* A sample structure to pass events from the PCNT
//  * interrupt handler to the main program.
//  */
// typedef struct {
//   int unit;         // the PCNT unit that originated an interrupt
//   uint32_t status;  // information on the event type that caused the interrupt
// } pcnt_evt_t;

// static void IRAM_ATTR quad_enc_isr(void *arg) {
//   uint32_t intr_status = PCNT.int_st.val;
//   int i;
//   pcnt_evt_t evt;
//   portBASE_TYPE HPTaskAwoken = pdFALSE;

//   for (i = 0; i < PCNT_UNIT_MAX; i++) {
//     if (intr_status & (BIT(i))) {
//       evt.unit = i;
//       /* Save the PCNT event type that caused an interrupt
//          to pass it to the main program */
//       evt.status = PCNT.status_unit[i].val;
//       PCNT.int_clr.val = BIT(i);
//       xQueueSendFromISR(pcnt_evt_queue, &evt, &HPTaskAwoken);
//       if (HPTaskAwoken == pdTRUE) {
//         portYIELD_FROM_ISR();
//       }
//     }
//   }
// }

// static void quad_enc_reset() { pcnt_counter_clear(PCNT_UNIT_0); }

// // https://www.esp32.com/viewtopic.php?t=4983
// static void quad_enc_counter_init(quad_encoder_mode enc_mode) {
//   pcnt_config_t pcnt_config = {
//       .pulse_gpio_num = PCNT_PULSE_GPIO,
//       .ctrl_gpio_num = PCNT_CONTROL_GPIO,
//       .channel = PCNT_CHANNEL_0,
//       .unit = PCNT_UNIT_0,
//       .pos_mode = PCNT_COUNT_INC,       // Count up on the positive edge
//       .neg_mode = PCNT_COUNT_DIS,       // Keep the counter value on the negative edge
//       .lctrl_mode = PCNT_MODE_KEEP,     // Reverse counting direction if low
//       .hctrl_mode = PCNT_MODE_REVERSE,  // Keep the primary counter mode if high
//       .counter_h_lim = PCNT_H_LIM_VAL,
//       .counter_l_lim = PCNT_L_LIM_VAL,
//   };

//   switch (enc_mode) {
//     case QUAD_ENC_MODE_1:
//       break;
//     case QUAD_ENC_MODE_2:
//       pcnt_config.neg_mode = PCNT_COUNT_DEC;
//       break;
//     case QUAD_ENC_MODE_4:
//       // Doesn't appear to be possible to handle 4X mode with the PCNT. THis mode requires the count to increment when the CONTROL input changes.
//       break;
//   }

//   pcnt_unit_config(&pcnt_config);
//   pcnt_set_filter_value(PCNT_UNIT_0, 100);
//   pcnt_filter_enable(PCNT_UNIT_0);

//   pcnt_event_enable(PCNT_UNIT_0, PCNT_EVT_ZERO);
//   pcnt_event_enable(PCNT_UNIT_0, PCNT_EVT_H_LIM);
//   pcnt_event_enable(PCNT_UNIT_0, PCNT_EVT_L_LIM);

//   /* Initialize PCNT's counter */
//   pcnt_counter_pause(PCNT_UNIT_0);
//   pcnt_counter_clear(PCNT_UNIT_0);

//   /* Register ISR handler and enable interrupts for PCNT unit */
//   pcnt_isr_register(quad_enc_isr, NULL, 0, NULL);
//   pcnt_intr_enable(PCNT_UNIT_0);

//   /* Everything is set up, now go to counting */
//   pcnt_counter_resume(PCNT_UNIT_0);
// }

// void quad_enc_task() {
//   int16_t count = 0;
//   pcnt_evt_t evt;
//   portBASE_TYPE res;
//   while (1) {
//     /* Wait for the event information passed from PCNT's interrupt handler.
//      * Once received, decode the event type and print it on the serial monitor.
//      */
//     res = xQueueReceive(pcnt_evt_queue, &evt, 1000 / portTICK_PERIOD_MS);
//     if (res == pdTRUE) {
//       pcnt_get_counter_value(PCNT_UNIT_0, &count);
//       printf("Event PCNT unit[%d]; cnt: %d\n", evt.unit, count);
//       if (evt.status & PCNT_STATUS_THRES1_M) {
//         printf("THRES1 EVT\n");
//       }
//       if (evt.status & PCNT_STATUS_THRES0_M) {
//         printf("THRES0 EVT\n");
//       }
//       if (evt.status & PCNT_STATUS_L_LIM_M) {
//         printf("L_LIM EVT\n");
//       }
//       if (evt.status & PCNT_STATUS_H_LIM_M) {
//         printf("H_LIM EVT\n");
//       }
//       if (evt.status & PCNT_STATUS_ZERO_M) {
//         printf("ZERO EVT\n");
//       }
//     } else {
//       pcnt_get_counter_value(PCNT_UNIT_0, &count);
//       printf("Current counter value :%d\n", count);
//     }
//   }
// }

// void quad_enc_init() { /* Initialize PCNT event queue and PCNT functions */
//   if (pcnt_evt_queue == NULL) {
//     pcnt_evt_queue = xQueueCreate(10, sizeof(pcnt_evt_t));
//     quad_enc_counter_init(QUAD_ENC_MODE_2);
//     xTaskCreate((TaskFunction_t)(&quad_enc_task), QUAD_ENC_NAME, QUAD_ENC_STACK, NULL, QUAD_ENC_PRIORITY, NULL);
//   }
// }

void quad_enc_init() { /* Initialize PCNT event queue and PCNT functions */

  pcnt_config_t r_enc_config = {.pulse_gpio_num = PCNT_PULSE_GPIO,   // Rotary Encoder Chan A (GPIO32)
                                .ctrl_gpio_num = PCNT_CONTROL_GPIO,  // Rotary Encoder Chan B (GPIO33)

                                .unit = PCNT_UNIT_0,
                                .channel = PCNT_CHANNEL_0,

                                .pos_mode = PCNT_COUNT_INC,  // Count Only On Rising-Edges
                                .neg_mode = PCNT_COUNT_DIS,  // Discard Falling-Edge

                                .lctrl_mode = PCNT_MODE_KEEP,     // Rising A on HIGH B = CW Step
                                .hctrl_mode = PCNT_MODE_REVERSE,  // Rising A on LOW B = CCW Step

                                .counter_h_lim = INT16_MAX,
                                .counter_l_lim = INT16_MIN};
  int r_enc_count;
  pcnt_unit_config(&r_enc_config);

  pcnt_set_filter_value(PCNT_UNIT_0, 250);  // Filter Runt Pulses
  pcnt_filter_enable(PCNT_UNIT_0);

  gpio_set_direction(GPIO_NUM_25, GPIO_MODE_INPUT);
  gpio_pullup_en(GPIO_NUM_25);  // Rotary Encoder Button

  gpio_pulldown_en(GPIO_NUM_32);
  gpio_pulldown_en(GPIO_NUM_33);

  pcnt_counter_pause(PCNT_UNIT_0);  // Initial PCNT init
  pcnt_counter_clear(PCNT_UNIT_0);
  pcnt_counter_resume(PCNT_UNIT_0);

  while (1) {
    pcnt_get_counter_value(PCNT_UNIT_0, &r_enc_count);
    ESP_LOGI("counter", "%d - button:%d", r_enc_count, gpio_get_level(GPIO_NUM_25));

    vTaskDelay(200 / portTICK_PERIOD_MS);  // Delay 1000msec & yield.
  }
}
