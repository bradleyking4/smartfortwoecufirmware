#pragma once

#include "driver/adc.h"

// esp32
// i2c
#define SDA GPIO_NUM_4
#define SCL GPIO_NUM_5

#define SPI_MISO GPIO_NUM_12
#define SPI_MOSI GPIO_NUM_13
#define SPI_CLK GPIO_NUM_14
#define SD_CS GPIO_NUM_15
#define CAN1_CS GPIO_NUM_417

#define CAN0_D GPIO_NUM_23
#define CAN0_R GPIO_NUM_22

#define GEARBOX_ENCODER_A GPIO_NUM_25
#define GEARBOX_ENCODER_B GPIO_NUM_33

#define GEARSTICK_IOEXP_INT GPIO_NUM_34

#define ADC_GEAR_POS ADC2_CHANNEL_9_GPIO_NUM
#define ADC_GEAR_MOTOR_CURRENT ADC2_CHANNEL_7_GPIO_NUM

#define ADC_WATER_TEMPERATURE ADC1_CHANNEL_4_GPIO_NUM
#define ADC_OIL_TEMP ADC1_CHANNEL_7_GPIO_NUM
#define ADC_ACCEL_1 ADC1_CHANNEL_3_GPIO_NUM
#define ADC_ACCEL_2 ADC1_CHANNEL_0_GPIO_NUM

// IO expander 1 (0x26) gear
#define GPA0_1 50
#define GPA1_1 51
#define GPA2_1 52
#define GPA3_1 53
#define GPA4_1 54
#define GPA5_1 55
#define GPA6_1 56
#define GPA7_1 57

#define GPB0_1 58
#define GPB1_1 59
#define GPB2_1 60
#define GPB3_1 61
#define GPB4_1 62
#define GPB5_1 63
#define GPB6_1 64
#define GPB7_1 65

// IO expander 2 (0x27) kelly
#define GPA0_2 70
#define GPA1_2 71
#define GPA2_2 72
#define GPA3_2 73
#define GPA4_2 74
#define GPA5_2 75
#define GPA6_2 76
#define GPA7_2 77

#define GPB0_2 78
#define GPB1_2 79
#define GPB2_2 80
#define GPB3_2 81
#define GPB4_2 82
#define GPB5_2 83
#define GPB6_2 84
#define GPB7_2 85

// DAC 0x34445
#define KELLY_DAC_THROTTLE 90
#define KELLY_DAC_BRAKE 91

// IO expander 1 (0x26)
#define GEARSTICK_5 GPB0_1
#define STOP_LIGHT GPB1_1 // Active Low
#define GEARSTICK_3 GPB2_1
#define GEARSTICK_9 GPB3_1
#define GEARSTICK_8 GPB4_1
#define STARTMOTOR GPB5_1
#define GEARSTICK_10 GPB6_1
#define GEARSTICK_4 GPB7_1

// #define GPA0_1
#define BREAK_SW GPA2_1
#define ACCEL_SW GPA1_1
#define REV_SW GPA4_1
#define FWD_SW GPA3_1
#define LOCKOUT GPA5_1
// #define GPA6_1
// #define GPA7_1

// IO expander 2 (0x27)
#define IO_1 GPB0_2
#define IO_2 GPB1_2
#define IO_3 GPB2_2
#define IO_4 GPB3_2
#define CONTACTOR_MAIN GPB4_2
#define KELLY_POWER GPB5_2
#define CONTACTOR_MOTOR_DRIVE GPB6_2
#define CONTACTOR_CHARGE GPB7_2

// #define GPA0_2
#define GEARBOX_MOTOR_DIR_A GPA1_2
#define GEARBOX_MOTOR_DIR_B GPA2_2
// #define GPA3_2
// #define GPA4_2
#define CONTACTOR_SPARE_1 GPA5_2
#define CONTACTOR_DCDC GPA6_2
#define CONTACTOR_SPARE_2 GPA7_2
