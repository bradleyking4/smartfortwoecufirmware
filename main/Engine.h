#pragma once

#include <stdint.h>

enum TempLight {
  off = 0,
  flashing = 1,
  solid = 2,
};

class Engine {
 private:
 public:
  const uint16_t NUMBER_OF_CELLS = 24;
  const float CELLS_MAX = 4.0 * NUMBER_OF_CELLS;
  const float CELLS_LOW = 3.7 * NUMBER_OF_CELLS;

  int rpm;
  int batteryLevel = 200;
  float temp;
  TempLight tempLight;
  bool oilLight;
  bool engineLight;
  bool batteryLight;

  Engine(/* args */);
  ~Engine();

  float map(float x, float in_min, float in_max, float out_min, float out_max);

  // 0x190
  int getRpm();
  int getTemp();

  // 0x300
  int getRpmSam();
};
