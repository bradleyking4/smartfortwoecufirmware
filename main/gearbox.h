#pragma once
#include "stdint.h"

#define GEAR_POSITIONS 7
#define GEAR_TOLLERENCE 0.25

#define REVERSE 0
#define NETURAL 1
#define FIRST 2
#define SECCOND 3
#define THIRD 4
#define FOURTH 5
#define FIFTH 6
#define SIXTH 7

class Gearbox {
 private:
  float voltPositions[GEAR_POSITIONS] = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6};
  uint8_t position = NETURAL;
  uint8_t newPosition = NETURAL;

  float measurePosition();
  void headToPosition(int nearestPos);

 public:
  uint8_t getPosition() {
    return position;
  }

  uint8_t getNewPosition() {
    return newPosition;
  }

  uint8_t getPositionForDash() {
    switch (position) {
      case REVERSE:
        return 0;
      case NETURAL:
        return 1;
      case FIRST:
        return 3;
      case SECCOND:
        return 4;
      case THIRD:
        return 5;
      case FOURTH:
        return 6;
      case FIFTH:
        return 7;
      case SIXTH:
        return 8;
    }
    return newPosition;
  }

  // get the position as per 0x300 packet structure
  uint8_t getPositionForSan() {
    if (position == REVERSE)
      return 7;
    else
      return position - 1;
  }

  uint8_t getNewPositionForSam() {
    if (newPosition == REVERSE)
      return 7;
    else
      return newPosition - 1;
  }

  bool changeUp() {
    if (position < SIXTH) {
      changeTo(++position);
      return true;
    } else {
      return false;
    }
  };

  bool changeDown() {
    if (position > FIRST) {
      changeTo(--position);
      return true;
    } else {
      return false;
    }
  };

  void changeTo(int8_t newPosition);

  Gearbox(/* args */);
  // ~Gearbox();
};

extern Gearbox gearbox;