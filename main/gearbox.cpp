#include "gearbox.h"

#include "Peripherals/motorDirver.h"
#include "VIRT_IO/virtual_IO.h"
#include "pinout.h"
#include "stddef.h"

Gearbox gearbox;

float Gearbox::measurePosition() { return IO_readADC(ADC_GEAR_POS); }

void Gearbox::headToPosition(int nearestPos) {
  float position = measurePosition();
  float direction = (voltPositions[nearestPos] - position);

  if (direction > 0) {
    while (measurePosition() < voltPositions[nearestPos]) {
      Motor_SetDirection(1);
      Motor_setDuty(30);
    }

  } else if (direction < 0) {
    while (measurePosition() > voltPositions[nearestPos]) {
      Motor_SetDirection(-1);
      Motor_setDuty(30);
    }
  }
  Motor_SetDirection(0);
}

Gearbox::Gearbox() {
  // float position = measurePosition();

  float minDifference = 10;
  // int indexOfPosition = 0;
  // for (int i = REVERSE; i <= FIFTH; i++) {
  //   float dist = abs(position - voltPositions[i]);
  //   if (dist < minDifference) {
  //     minDifference = dist;
  //     indexOfPosition = i;
  //   }
  // }

  if (minDifference > GEAR_TOLLERENCE) {
    // headToPosition(indexOfPosition);
  }
  position = NETURAL;
  newPosition = NETURAL;
  //  headToPosition(FIRST);
}

void Gearbox::changeTo(int8_t _newPosition) {
  if (newPosition != _newPosition) {  /// we need to change
    newPosition = _newPosition;
    vTaskDelay(50);
  }
  position = newPosition;

  // if (newPosition < position) {
  //   headToPosition()
  // } else {
  //   /* code */
  // }
}